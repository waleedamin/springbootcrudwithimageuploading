package com.waleed.springbootcurd.controllers;

import java.lang.Thread.State;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.waleed.springbootcurd.Entities.Employee;
import com.waleed.springbootcurd.services.CrudService;

@RestController
@RequestMapping("/api/crude")
public class CrudeController {
	
	@Autowired
	CrudService crs;
	
	@PostMapping("/createemployee")
    public ResponseEntity<Employee> add(@RequestBody Employee employee) {
        crs.Create(employee);
        return new ResponseEntity<Employee>(employee, HttpStatus.OK);
    }
	
	
	@PutMapping("employee/update/{id}")
	public ResponseEntity<Employee> update(@RequestBody Employee employee, @PathVariable int id ) {

	       crs.update(employee, id);

	       return new ResponseEntity<Employee>(employee, HttpStatus.OK);
	    }
	
	
		@DeleteMapping("employee/delete/{id}")
		public String delete(@PathVariable int id ) {

	       crs.delete(id);

	       return "Deleted";
	    }
		
		
		@GetMapping("/getemployees")
	    public ResponseEntity<?> list() {        

	        return new ResponseEntity<>(crs.list(), HttpStatus.OK);

	    }

}
