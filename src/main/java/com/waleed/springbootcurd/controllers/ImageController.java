package com.waleed.springbootcurd.controllers;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.waleed.springbootcurd.Entities.ImageEntity;
import com.waleed.springbootcurd.repositories.ImageRepository;
import com.waleed.springbootcurd.response.ImageResponse;
import com.waleed.springbootcurd.services.ImageServices;

import java.io.IOException;
import java.util.Optional;

@RestController
@RequestMapping("/api/image")
public class ImageController {

    @Autowired
    ImageRepository imageRepository;

    @Autowired
    private ImageServices fileStorageService;

    @PostMapping(value="/uploadFile")
    public ImageResponse uploadFile(@RequestParam("file")  MultipartFile file) {
    	System.out.println(" hiiii ");
        ImageEntity fileName = fileStorageService.storeFile(file);

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
            .path("/downloadFile/")
            .path(fileName.getFileName())
            .toUriString();

        return new ImageResponse(fileName.getFileName(), fileDownloadUri, file.getContentType(), file.getSize());
    }
}