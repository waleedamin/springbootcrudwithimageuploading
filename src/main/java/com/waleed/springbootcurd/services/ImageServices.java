package com.waleed.springbootcurd.services;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.util.StringUtils;

import com.waleed.springbootcurd.Entities.ImageEntity;
import com.waleed.springbootcurd.exceptions.FileStorageException;
import com.waleed.springbootcurd.repositories.ImageRepository;

@Service
public class ImageServices {
	
	@Autowired
    private ImageRepository imageservice;

    public ImageEntity storeFile(MultipartFile file) {
        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            // Check if the file's name contains invalid characters
            if (fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            ImageEntity dbFile = new ImageEntity(fileName, file.getContentType(), file.getBytes());

            return imageservice.save(dbFile);
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    public ImageEntity getFile(String fileId) throws FileNotFoundException {
    	
    	
      			
    	return imageservice.findById(fileId).orElseThrow(()-> new FileNotFoundException("File not found with id " + fileId));
    }

}
