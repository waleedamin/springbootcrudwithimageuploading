package com.waleed.springbootcurd.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.waleed.springbootcurd.Entities.ImageEntity;

@Repository
public interface ImageRepository extends JpaRepository<ImageEntity, String> {

	
	
}
