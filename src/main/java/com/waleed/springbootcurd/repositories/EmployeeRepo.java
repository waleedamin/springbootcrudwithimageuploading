package com.waleed.springbootcurd.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.waleed.springbootcurd.Entities.Employee;

@Repository
public interface EmployeeRepo extends JpaRepository<Employee, Integer>{
	


}
